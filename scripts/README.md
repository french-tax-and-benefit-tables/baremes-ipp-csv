# Generate CSV from YAML parameters files

## Prerequisites

1. Clone the YAML parameters repository:

```
git clone https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-yaml.git
```

2. Install the OpenFisca package wrapping these parameters:

```
cd baremes-ipp-yaml/.openfisca
pip install --editable .
```

## Run the script

Back in the `baremes-ipp-yaml` repository, run the `generate_csv.py` script

```
cd baremes-ipp-csv
./scripts/generate_csv.py
```

The CSV parameters files will generated in this directory.