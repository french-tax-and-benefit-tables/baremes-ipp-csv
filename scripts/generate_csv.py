#!/usr/bin/env python

import argparse
import logging
import os

from openfisca_baremes_ipp import CountryTaxBenefitSystem
from csv_tools import export_node_to_csv


tbs = CountryTaxBenefitSystem()
log = logging.getLogger(__name__)
root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir)) 

def main():
  argparser = argparse.ArgumentParser()
  argparser.add_argument("-v", "--verbose", help = "increase output verbosity", action = "store_true")
  args = argparser.parse_args()

  logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING)

  for node in tbs.parameters.children.values():
    export_node_to_csv(node, root_dir)


if __name__ == "__main__":
    main()
